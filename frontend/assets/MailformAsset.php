<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Contact page asset bundle.
 */
class MailformAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/mailform.css'
    ];
    public $js = [

    ];
    public $depends = [
//        'yii\web\YiiAsset',
//        'yii\bootstrap\BootstrapAsset',
    ];
}
