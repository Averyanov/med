<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        '/css/grid.css',
        '/css/style.css',
        '/css/search.css',
        '/css/subscribe-form.css',
        '/css/mailform.css',
    ];
    public $js = [
        '/js/jquery.js',
        '/js/device.min.js',
        '/js/script.js',

    ];
    public $depends = [
//        'yii\web\YiiAsset',
//        'yii\bootstrap\BootstrapAsset',
    ];
}
