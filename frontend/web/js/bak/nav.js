var toogleNavbar = function (e) {
    var condition = $(window).scrollTop() < 150 || e.originalEvent.wheelDelta > 0;

    $('.navbar').toggleClass('show', condition);
    $('#to_top').toggleClass('stash', condition);
};

$(window).bind('mousewheel', toogleNavbar);     // IE Opera Safari
$(window).bind('DOMMouseScroll', toogleNavbar); // Firefox
$(window).bind('DOMContentLoaded', toogleNavbar);

$('#to_top').click(function () {
    $('html, body').animate({scrollTop : 0},800, 'swing');
    $(this).addClass('stash')
    $('.navbar').addClass('show');
});