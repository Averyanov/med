var scrollColumn = function (e) {
    var scrollStep = e.originalEvent.wheelDelta;
    var scrollDirect = scrollStep < 0;

    var column = getColumn(e.target);

    if(column) e.preventDefault();

    var columnTop = parseInt($(column).css('top'));
    var columnHeight = parseInt($(column).css('height'));

    var currentPosition = (columnTop + scrollStep);

    // console.log((columnHeight + currentPosition - window.innerHeight));
    $(column).css({
        'position' : 'relative',
        'top' : ((currentPosition < 0) && ((columnHeight + currentPosition - window.innerHeight) > 200)) ? currentPosition + 'px' : false
    })
};

var getColumn = function(target)
{
    var column = $(target).parents('.main-column');
    return $(column).hasClass('main-column') ? $(column) : false;
};

$(window).bind('mousewheel', scrollColumn);     // IE Opera Safari
$(window).bind('DOMMouseScroll', scrollColumn); // Firefox