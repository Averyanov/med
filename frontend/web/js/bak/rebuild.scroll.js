var rebuildColumn = function () {
    var map = getColumnMap;
    var condition = true;

    for(var i = 0; map[i].getBoundingClientRect().bottom < 0 && condition; i++)
    {
        map[i].classList.add('hidden-md');
        // console.log(map);
        // var HWidth = getCurrentWidth.call(map[i]);
        // var CWidth = getCurrentWidth.call(map[map.length-1]);
        // var CClass = getCurrentClass.call(map[map.length-1]);
        //
        // map[map.length-1].classList.remove(CClass);
        // var NClass = 'col-md-' + (CWidth + HWidth);
        //
        // map[map.length-1].classList.add(NClass);
        //
        // map[i].classList.add('hidden');
        // console.log(map[i].getBoundingClientRect());
    }
};

var getColumnMap = function () {
    var list = document.getElementsByClassName('main-column');
    var Slist = []; // order by height

    for(var i = 0; typeof list[i] == 'object'; i++)
    {
        var tmp; // bubule var
        Slist[i] = !Slist[i] ? list[i] : Slist[i];

        if(i > 0 && Slist[i].clientHeight <= Slist[i-1].clientHeight)
        {
            tmp = Slist[i];
            Slist[i] = Slist[i-1];
            Slist[i-1] = tmp;
            i -= 2;
        }
    }

    return Slist;
}();

// var getCurrentClass = function()
// {
//     for(var i = 0; this.classList[i].indexOf('col-md-') > -1; i++)
//     {
//         return this.classList[i];
//     }
// };
//
// var getCurrentWidth = function()
// {
//     for(var i = 0; this.classList[i].indexOf('col-md-') > -1; i++)
//     {
//         return 1*this.classList[i].slice(-1);
//     }
// };

$(window).bind('mousewheel', rebuildColumn);     // IE Opera Safari
$(window).bind('DOMMouseScroll', rebuildColumn); // Firefox
$(window).bind('DOMContentLoaded', rebuildColumn);