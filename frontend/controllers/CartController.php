<?php
namespace frontend\controllers;

use common\models\Lection;
use Yii;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\data\ActiveDataProvider;

use common\models\Cart;
use common\models\Payment;

/**
 * Site controller
 */
class CartController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['remove', 'add', 'index'],
                'rules' => [
                    [
                        'actions' => ['add', 'remove', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'add' => ['post'],
                    'remove' => ['post'],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        Url::remember(Yii::$app->request->referrer);
        return parent::beforeAction($action);
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        $cart = Yii::$app->user->identity->currentCart;

        $dataProvider = new ActiveDataProvider([
            'query' => $cart->getLections()
        ]);

        return $this->render('index',[
            'dataProvider' => $dataProvider,
            'cart' => $cart,
            'payment' => $dataProvider->count ? (new Payment()) : false
        ]);
    }

    public function actionAdd()
    {
        $user = Yii::$app->user->identity;
        $lection = Lection::findOne(Yii::$app->request->post('id'));

        // if user doesn`t have cart with status 0,
        // create and link to user
        // but $model->link always return void
        // that`s why again return already exist cart with status 0
        $cart = $user->currentCart ?: (new Cart())->link('user', $user) ?: $user->currentCart;

        $cart->link('lections', $lection);

        return $this->redirect(['cart/index']);
    }

    public function actionRemove()
    {
        $lection = Lection::findOne(Yii::$app->request->post('id'));
        $cart = Yii::$app->user->identity->currentCart;

        $cart->unlink('lections', $lection, true);

        return $this->redirect(['cart/index']);
    }
}
