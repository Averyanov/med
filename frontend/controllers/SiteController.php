<?php
namespace frontend\controllers;

use common\models\Author;
use common\models\Blog;
use common\models\Subscribe;
use Yii;
use yii\base\InvalidParamException;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

use common\models\LoginForm;
use common\models\Lection;
use common\models\Mailer;

use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
//                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $lectionDataProvider = new ActiveDataProvider([
            'query' => Lection::find()->where(['id' => [1,2,3]])
        ]);

        $authorDataProvider = new ActiveDataProvider([
            'query' => Author::find()
        ]);

        $articleDataProvider = new ActiveDataProvider([
            'query' => Blog::byOwner()->getArticles()
        ]);

        $lectionOnMain = Blog::byOwner()->getArticles()->where(['id' => 3])->one();
        $videoOnMain = Lection::find()->where(['id' => 1])->one();

        return $this->render('index',[
            'lectionDataProvider'   => $lectionDataProvider,
            'authorDataProvider'    => $authorDataProvider,
            'articleDataProvider'   => $articleDataProvider,
            'lectionOnMain'         => $lectionOnMain,
            'videoOnMain'           => $videoOnMain
        ]);
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack(Url::previous());
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending your message.');
            }

            return $this->refresh();
        } else {
            return $this->render('/contact/view', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    public function actionCabinet()
    {
        if (Yii::$app->user->isGuest) {
            Url::remember(Yii::$app->request->url);
            return $this->redirect(Url::toRoute(['site/login']));
        }

        $user = Yii::$app->user->identity;
        $lectionDataProvider = new ActiveDataProvider([
            'query' => $user->getAccess()
        ]);

        return $this->render('/user/cabinet',[
            'lectionDataProvider' => $lectionDataProvider
        ]);
    }

    public function actionSubscribe()
    {
        $post = Yii::$app->request->post();
        $model = Yii::$app->mailer;

        if($post && $model->load($post))
        {
            $resp = $model->compose(
                'Обновления medfiles.me',       //subject
                ['template' => 'subscribe']     //message options [template,...custom variables]
            )
                ->send();
            $subscribe = new Subscribe();

            $subscribe->attributes = [
                'username'  => $model->getUsername(),
                'useremail' => $model->getUseremail(),
                'sended'    => $resp->statusCode()
            ];

            $subscribe->save();

            return $this->redirect(Url::toRoute(['site/index']));
        }

        return $this->render('subscribe',[]);
    }
}
