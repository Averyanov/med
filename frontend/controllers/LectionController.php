<?php
namespace frontend\controllers;

use common\models\Author;
use Yii;
use common\models\Lection;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\data\ActiveDataProvider;
use yii\helpers\VarDumper;

/**
 * Site controller
 */
class LectionController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Lection::find(),
            'pagination' => [
                'pageSize' => 3
            ]
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider
        ]);
    }

    public function actionView($url)
    {
        $model = Lection::findOne(['url' => $url]);

        $user = Yii::$app->user->identity;

        $dataProvider = new ActiveDataProvider([
            'query' => Lection::find()
        ]);

        return $this->render($user && $user->getIsAccess($model->id) ? 'view' : 'preview', [
            'model' => $model,
            'dataProvider' => $dataProvider
        ]);
    }
}
