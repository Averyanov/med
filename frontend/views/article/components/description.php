<?
use yii\helpers\Url;
?>
<h3><?= $model->name ?></h3>
<div class="short_description">
    <?= $model->getCut('short_description', 300) ?>
</div>
<a href="<?= Url::toRoute(['article/view', 'url' => $model->url]) ?>" class="btn">Далее</a>
