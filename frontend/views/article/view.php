<?
use yii\widgets\ListView;
?>
<?
$this->title = $model->meta->title;
$this->params['breadcrumbs'][] = [
    'label' => 'Блог' ,
    'url' => ['blog/view', 'url' => $model->blog->url]
];
$this->params['breadcrumbs'][] = $this->title;
?>
<section>
    <div class="container">
        <div class="row">
            <div class="grid_8">
<!--                <h1>--><?//= $model->h1 ?><!--</h1>-->
                <p class='small text-muted'><?= date('Y-m-d h:m:i') ?></p>
                <img src='<?= $model->image ?>' alt='<?= $model->name ?>' class=''>
                <br>
                <strong><?= $model->blog->author->name ?> <?= $model->blog->author->surname ?></strong>
                <p class="h4 text-muted"><em><?= $model->name ?></em></p>
                <p class="lead content"><?= $model->content ?></p>
            </div>
            <div class="grid_1"></div>
            <div class="grid_3">
                <?=
                ListView::widget([
                    'dataProvider' => $lectionDataProvider,
                    'layout' => '{items}',
                    'options' => ['class' => 'row'],
                    'itemOptions' => ['class' => 'col-md-10 col-md-offset-1 lection-side'],
                    'itemView' => '/lection/side'
                ])
                ?>
            </div>
        </div>
    </div>
</section>