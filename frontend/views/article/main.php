<?
use yii\helpers\Url;
?>

<p class='h3'><?= $model->name ?></p>
<p class="h4"><?= $model->h1 ?></p>
<p class="text-right"><?= $model->getCut('short_description', 100) ?></p>
<blockquote>
    <p class="text-info"><?= $model->getCut('full_description', 200) ?></p>
    <footer><?= $model->blog->author->name ?></footer>
</blockquote>
<p>
    <a class="btn btn-default" href='<?= Url::toRoute(['article/view', 'url' => $model->url]) ?>'>
        Далее &raquo;
    </a>
</p>