<?
use yii\helpers\Url;
?>
<p class="h3"><?= $model->name ?></p>
<p class="text-muted"><?= date('Y-m-d h:m:i') ?></p>
<img src='<?= $model->image ?>' alt='<?= $model->name ?>' class='pull-left blog-image'>
<p class="text-warning full-description"><?= $model->full_description ?></p>
<p class="lead content"><?= $model->getCut('content', 400) ?></p>
<a href='<?= Url::toRoute(['article/view', 'url' => $model->url]) ?>' class='btn btn-default'>Далее >> </a>
