<?
use yii\helpers\Url;
?>

<h1>
    <a href='<?= Url::toRoute(['article/view', 'url' => $model->url]) ?>'><?= $model->h1 ?></a>
</h1>
<p class="h4"><?= $model->name ?></p>
<p class="text-muted"><?= $model->getCut('short_description', 100) ?></p>
<p class="lead"><?= $model->getCut('full_description', 100) ?></p>
