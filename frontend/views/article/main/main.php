<?
//\yii\helpers\VarDumper::dump($model,10,1);die;
use yii\helpers\Url;
?>
<time datetime="<?= Yii::$app->formatter->asDate($model->date, 'yyyy-MM-dd') ?>">
    <?= Yii::$app->formatter->asDate($model->date, 'dd/MM/yyyy') ?>
</time>
<h5 class="fixed-height-90">
    <a href="<?= Url::toRoute(['article/view', 'url' => $model->url]) ?>">
        <?= $model->name ?>
    </a>
</h5>
<div class="short_description">
    <?= $model->getCut('short_description', 200) ?>
</div>
<a href="<?= Url::toRoute(['article/view', 'url' => $model->url]) ?>" class="btn">Читать</a>
