<?
$this->title = $model->meta->title;
//$this->params['breadcrumbs'][] = $this->title;
?>
<section class="well1">
    <div class="container">
        <h3><?= $this->title ?></h3>
        <p class="lead"><?= $model->content ?></p>
    </div>
</section>