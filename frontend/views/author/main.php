<?
use yii\helpers\Url;
?>
<a class='h3' href='<?= Url::toRoute(['author/view', 'url' => $model->url]) ?>'>
    <?= $model->name ?>
</a>
<p class='h4 text-right'><?= $model->surname ?></p>
<img src='<?= $model->image ?>' alt='' class='main-author-image'>
<p><?= $model->getCut('about', 50) ?></p>