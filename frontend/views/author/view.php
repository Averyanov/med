<?
use yii\helpers\Url;
?>
<?
$this->title = $model->meta->title;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-md-12">
        <h1><?= $model->name ?></h1>
        <p class="h2"><?= $model->surname ?></p>
        <div class="row">
            <div class="col-md-5 col-md-offset-1">
                <img src='<?= $model->image ?>' alt='<?= $model->name ?>' class='pull-left'>
            </div>
            <div class="col-md-3 col-md-offset-1">
                <em class='pull-right'><?= $model->about ?></em>
            </div>
        </div>
    </div>
</div>
