<?
use yii\widgets\ListView;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\helpers\Html;
use common\models\Lection;
?>
<?=
ListView::widget([
    'dataProvider'  => $menu->getDataProvider(),
    'layout'        => '{items}',
    'options'       => [
        'tag'           => 'ul',
        'class'         => 'sf-menu',
        'data-type'     => 'navbar'
    ],
    'itemOptions'   => [
        'tag'    => null,
    ],
    'itemView'  => function($model)
    {
        $url = Json::decode($model->route);
        $inner = Html::a(
            $model['is_html'] ? $model['label'] :  mb_strtoupper($model['label']),
            Url::toRoute($url)
        );

        if($model->id == 3)
        {
            $inner.= Html::ul(
                Lection::find()->all(),
                ['item' => function($item){
                    $link = Html::a($item->name,Url::toRoute(['lection/view', 'url' => $item->url]));
                    return Html::tag('li',$link);
                }]
            );
        }

        return Html::tag(
            'li',
            $inner,
            ['class' => Yii::$app->request->url == Url::toRoute($url) ? 'active' : '']
        );
    }]
);