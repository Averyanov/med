<?php

/* @var $this \yii\web\View */
/* @var $content string */

//use yii\bootstrap\Nav;
//use yii\bootstrap\NavBar;
//use yii\widgets\Breadcrumbs;

//use frontend\assets\NavAsset;
////use frontend\assets\MainScrollAsset;
//use common\widgets\Alert;

use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\widgets\ListView;

use frontend\assets\AppAsset;

use common\models\Menu;

AppAsset::register($this);

//NavAsset::register($this);
////MainScrollAsset::register($this);
//?>
<?/*?>
<?php //$this->beginPage() ?>
<!--<!DOCTYPE html>-->
<!--<html lang="--><?//= Yii::$app->language ?><!--">-->
<!--<head>-->
<!--    <meta charset="--><?//= Yii::$app->charset ?><!--">-->
<!--    <meta http-equiv="X-UA-Compatible" content="IE=edge">-->
<!--    <meta name="viewport" content="width=device-width, initial-scale=1">-->
<!--    --><?//= Html::csrfMetaTags() ?>
<!--    <title>--><?//= Html::encode($this->title) ?><!--</title>-->
<!--    --><?php //$this->head() ?>
<!--</head>-->
<!--<body>-->
<?php //$this->beginBody() ?>
<!--<div class="wrap">-->
<!--    --><?php
//    NavBar::begin([
//        'brandLabel' => Html::img('/image/header.png'),
//        'brandUrl' => Yii::$app->homeUrl,
//        'options' => [
//            'class' => 'navbar-fixed-top navbar-default',
//        ]
//
//    ]);
//    $menu = new Menu();
//    $menuItems = $menu->getList('right');
//    if (Yii::$app->user->isGuest) {
//        $menuItems[] = [
//            'label' => Html::tag('span', '', ['class' => 'glyphicon glyphicon-user']),
//            'url' => ['/site/signup']
//        ];
//        $menuItems[] = [
//            'label' => Html::tag('span', '', ['class' => 'glyphicon glyphicon-log-in']),
//            'url' => ['/site/login']
//        ];
//    } else {
//        $menuItems[] = '<li>'
//            . Html::beginForm(['/site/logout'], 'post')
//            . Html::submitButton(
//                Html::tag('span', null, ['class' => 'glyphicon glyphicon-log-out']),
//                ['class' => 'btn btn-link logout']
//            )
//            . Html::endForm()
//            . '</li>';
//        $menuItems[] = [
//            'label' => Html::tag('span', ' '.Yii::$app->user->identity->username, ['class' => 'glyphicon glyphicon-education text-capitalize']),
//            'url' => ['/site/cabinet']
//        ];
//        $menuItems[] = Yii::$app->user->identity->currentCart->getCartItems()->count()
//            ? [
//                'label' => Html::tag('span', ' '.Yii::$app->user->identity->currentCart->getCartItems()->count(), ['class' => 'glyphicon glyphicon-shopping-cart']),
//                'url' => ['/cart']
//            ]
//            : '';
//    }
//    echo Nav::widget([
//        'options' => ['class' => 'navbar-nav navbar-left'],
//        'items' => $menu->getList('left'),
//        'encodeLabels' => false,
//    ]);
//    echo Nav::widget([
//        'options' => ['class' => 'navbar-nav navbar-right'],
//        'items' => $menuItems,
//        'encodeLabels' => false,
//    ]);
//    NavBar::end();
//    ?>
<!---->
<!--    <div class="container">-->
<!--        --><?//= Breadcrumbs::widget([
//            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
//            'homeLink' => ['label' => 'На главную', 'url' => Yii::$app->homeUrl]
//        ]) ?>
<!--        --><?//= Alert::widget() ?>
<!--        --><?//= $content ?>
<!--    </div>-->
<!--    <button id="to_top" type=button class="btn btn-warning btn-lg to-top">-->
<!--        <span class="glyphicon glyphicon-arrow-up" aria-hidden=true></span>-->
<!--    </button>-->
<!--</div>-->
<!--<footer class="footer">-->
<!--    <div class="container">-->
<!--        <p class="pull-left">&copy; My Company --><?//= date('Y') ?><!--</p>-->
<!---->
<!--        <p class="pull-right">--><?//= Yii::powered() ?><!--</p>-->
<!--    </div>-->
<!--</footer>-->
<!---->
<?php //$this->endBody() ?>
<!--</body>-->
<!--</html>-->
<?php //$this->endPage() ?>

<!--__________________-->
<!--new layout-->
<!--__________________-->
<?*/?>
<? $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="utf-8">
    <meta name="format-detection" content="telephone=no">
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>

    <!--[if lt IE 9]>
    <html class="lt-ie9">
    <div style="clear: both; text-align:center; position: relative;"><a href="http://windows.microsoft.com/en-US/internet-explorer/.."><img src="images/ie8-panel/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div>
    </html>
    <script src="js/html5shiv.js"></script>
    <![endif]-->

</head>
<body>
<? $this->beginBody() ?>
<div class="page">
    <!--
    ========================================================
                                HEADER
    ========================================================


    -->
    <header>
        <div id="stuck_container" class="stuck_container">
            <div class="wr">
                <div class="brand">
<!--                    <p class="brand_slogan">каридо блог</p>-->
                    <h1 class="brand_name"><a href="/">medfiles</a></h1>
                </div>
                <nav class="nav">
                    <?
                        $menu = new Menu();
                    ?>
                    <?=
                        $this->render('/layouts/menu/wrapper', ['menu' => $menu])
                    ?>
                </nav>
                <a href="#" class="search-form_toggle"></a>
            </div>
            <form action="search.php" method="GET" accept-charset="utf-8" class="search-form">
                <div class="container">
                    <label class="search-form_label">
                        <input type="text" name="s" autocomplete="off" placeholder="Поиск.." class="search-form_input"><span class="search-form_liveout"></span>
                    </label>
                    <button type="submit" class="search-form_submit fa-search"></button>
                </div>
            </form>
        </div>
    </header>
    <!--
    ========================================================
                                CONTENT
    ========================================================
    -->
        <main>
            <?= $content ?>
        </main>
        <?/* <div class="hart"></div> */?>
    <!--
    ========================================================
                                FOOTER
    ========================================================
    -->
    <footer>
        <section class="mobile-center">
            <div class="container">
                <div class="row">
                    <div class="grid_4">
                        <?= $this->render('/feedback/footer/form') ?>
                    </div>
                    <div class="preffix_1 grid_3">
                        <h6>
                            <a href="<?= Url::toRoute(['site/contact']) ?>">
                                Связаться с нами
                            </a>
                        </h6>
                        <address>8901 Marmora Road <br/>Glasgow, DO4 89GR.</address>
                        <dl>
                            <dt>Freephone:</dt>
                            <dd> <a href="callto:#">+1 800 559 6580</a></dd>
                        </dl>
                        <dl>
                            <dt>Telephone:</dt>
                            <dd> <a href="callto:#">+1 959 603 6035</a></dd>
                        </dl>
                        <dl>
                            <dt>FAX:</dt>
                            <dd> <a href="callto:#">+1 504 889 9898</a></dd>
                        </dl>
                        <dl>
                            <dt>E-mail:</dt>
                            <dd><a href="mailto:#">mail@demolink.org</a></dd>
                        </dl>
                    </div>
                    <div class="grid_4">
                        <h6>Мы в соцсетях</h6>
                        <ul class="inline-list">
<!--                            <li><a href="#" class="fa-google-plus"></a></li>-->
<!--                            <li><a href="#" class="fa-twitter"></a></li>-->
                            <li><a href="https://www.facebook.com/medfiles.me/" class="fa-facebook" target="_blank"></a></li>
                            <li><a href="https://www.youtube.com/channel/UCUxa02MyAA5isY745sFG_Gw" class="fa-youtube" target="_blank"></a></li>
<!--                            <li><a href="#" class="fa-pinterest"></a></li>-->
<!--                            <li><a href="#" class="fa-linkedin"></a></li>-->
                        </ul>
                        <br>
                        <h5>
                            <a href="<?= Url::toRoute(['page/view', 'url' => 'denial']) ?>" class="pp_link">Пользовательское соглашение</a>
                        </h5>
                    </div>
                </div>
            </div>
        </section>
        <section class="copyright">
<!--            <div class="container">© <span id="copyright-year"></span>&nbsp;Auerbach. cardiology clinic.&nbsp;<a href="index-6.html">Privacy policy</a>-->
            </div>
        </section>
    </footer>
</div>

<?php $this->endBody() ?>
</body>
</html>

<? $this->endPage() ?>