<?

use yii\widgets\ListView;

?>

<?
$this->title = 'Кабинет';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="container">
    <?=
    ListView::widget([
        'dataProvider'     => $lectionDataProvider,
        'layout'           => '{items}',
        'options'          => [
            'class' => 'row'
        ],
        'itemOptions'      => [
            'class' => 'grid_3',
            'style' => 'overflow: hidden'
        ],
        'itemView'         => 'lection-item',
        'emptyText'        => 'Список доступных лекций пуст...',
        'emptyTextOptions' => [
            'tag' => 'h4',
        ]
    ])
    ?>
</div>
