<?
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;
?>

<h6>Подпишись на обновления</h6>
<div class="subscribe-form">
    <? ActiveForm::begin([
        'action'    => Url::toRoute(['site/subscribe']),
        'id'        => 'subscribe-form',
    ]); ?>


    <?=
    Html::label(
            Html::textInput('username', 'Имя').'',
//                '<span class="error">*Invalid name.</span>
//                <span class="success">Subscription request sent!</span>',
            'name',
            ['class' => 'name'])
    ?>

    <?=
    Html::label(
            Html::textInput('useremail', 'Email').'',
//                '<span class="error">*Invalid email.</span>
//                <span class="success">Subscription request sent!</span>',
            'email',
            ['class' => 'email'])
    ?>

    <?=
    Html::textInput(null,'Подпишись на обновления',['type' => 'submit', 'data-type' => 'submit'])
    ?>

    <? ActiveForm::end(); ?>
</div>


<!--<form id="subscribe-form" class="subscribe-form">-->
<!--    <label class="name">-->
<!--        <input type="text" value="Имя"><span class="error">*Invalid name.</span><span class="success">Subscription request sent!</span>-->
<!--    </label>-->
<!--    <label class="email">-->
<!--        <input type="email" value="Email"><span class="error">*Invalid email.</span><span class="success">Subscription request sent!</span>-->
<!--    </label><a data-type="submit" href="#">Отправить</a>-->
<!--</form>-->
