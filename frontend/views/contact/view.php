<?

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use frontend\assets\MailformAsset;
?>

<?
$this->title = 'Contact';
?>

<?
MailformAsset::register($this);
?>

<section class="well1">
    <div class="container">
        <h3>Контактная форма</h3>
        <form method="post" action="bat/rd-mailform.php" class="mailform">
            <input type="hidden" name="form-type" value="contact">
            <fieldset class="row">
                <label class="grid_4">
                    <input type="text" name="name" placeholder="Имя" data-constraints="@LettersOnly @NotEmpty">
                </label>
                <label class="grid_4">
                    <input type="text" name="phone" placeholder="Телефон" data-constraints="@Phone">
                </label>
                <label class="grid_4">
                    <input type="text" name="email" placeholder="Email" data-constraints="@Email @NotEmpty">
                </label>
                <label class="grid_12">
                    <textarea name="message" placeholder="Текст сообщения" data-constraints="@NotEmpty"></textarea>
                </label>
                <div class="mfControls grid_12">
                    <button type="submit" class="btn2">Отправить</button>
                </div>
            </fieldset>
        </form>
    </div>
</section>
