<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ResetPasswordForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Reset password';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-reset-password">

    <div class="container">
        <div class="row">
            <div class="site-login">
                <h4 class="tittle"><?= Html::encode($this->title) ?></h4>
                <p style="margin-bottom: 10px">Please choose your new password:</p>

                <?php $form = ActiveForm::begin(['id' => 'reset-password-form']); ?>

                <label for="password">
                    <?= $form->field($model, 'password')->passwordInput([
                        'autofocus' => true,
                        'class'     => 'input-text'
                    ])->label(false) ?>
                </label>

                <div class="btn-wrapper">
                    <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
                </div>

                <?php ActiveForm::end(); ?>

            </div>
        </div>
    </div>
</div>
