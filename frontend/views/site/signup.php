<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Signup';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-signup">
    <div class="container">
        <div class="row">
            <div class="site-login">
                <h4 class="tittle"><?= Html::encode($this->title) ?></h4>
                <p style="margin-bottom: 10px">Please fill out the following fields to signup:</p>


                <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>

                <?= $form->field($model, 'username')->textInput(['autofocus' => true, 'class' => 'input-text'])->label('Логин') ?>

                <?= $form->field($model, 'email')->textInput(['class' => 'input-text'])->label('E-mail') ?>

                <?= $form->field($model, 'password')->passwordInput(['class' => 'input-text'])->label('Пароль') ?>

                <div class="btn-wrapper">
                    <?= Html::submitButton('Зарегистрироваться', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
                </div>

                <?php ActiveForm::end(); ?>

                <div style="text-align: center; font-style: italic; font-size: 80%;">
                    Уже зарегистрированы? <a href="/site/cabinet" style="border-bottom: 1px dotted;">Войти в кабинет</a>
                </div>
            </div>
        </div>
    </div>
</div>
