<?php

/* @var $this yii\web\View */

use yii\widgets\ListView;
use yii\helpers\Url;

$this->title = 'Главная';

?>
<?/*<section class="camera_container">
    <div class="container">
        <div class="row">
            <div class="grid_12">
                <?= $this->render('subscribe_form') ?>
            </div>
        </div>
    </div>
</section>*/?>
<section data-mobile="true" class="camera_container left" id="subcribe_home">
    <div class="container">
        <div class="row">
            <h2 class="site-index">
                <em>Экспертная медицинская <br> информация для каждого</em>
            </h2>
            <article class="grid_3">
                <a href="<?= Url::toRoute(['site/subscribe']) ?>" class="btn site-index">1</a>
            </article>
            <div class="grid_3">
                <a href="" class="btn site-index">2</a>
            </div>
        </div>
    </div>
    <?/*<div class="container">
        <div class="row">
            <div class="grid_4" style="visibility: hidden">
                <div class="promo-subscribe-bg">
                    <div class="promo-subscribe-text">
                        <div class="custom_index_site" style="text-align: center;">Здравствуйте!</div>
                        <div class="custom_index_site"><em>Оформляя подписку - Вы получаете доступ ко всем
                                курсам, а также к их материалам и тестам.</em>
                    </div>
                    </div>
                    <div class="promo-post-card">
                        <?= $this->render('subscribe_form') ?>
                    </div>
                </div>
            </div>
        </div>
    </div>*/?>
</section>
<section class="well1">
    <div class="container">
        <div class="row">
            <div class="grid_12">
                <h5 class="title_section_home">Статьи</h5>
            </div>
        </div>
        <?=
        ListView::widget([
            'dataProvider' => $articleDataProvider,
            'layout' => '{items}',
            'options' => [
                'class' => 'row'
            ],
            'itemOptions' => [
                'class' => 'grid_4',
                'tag'   => 'article'
            ],
            'itemView' => '/article/main/main'
        ])
        ?>
    </div>
</section>
<section class="well bg-color">
    <div class="container">
        <div class="row">
            <div class="grid_12">
                <h5 class="title_section_home">Блог</h5>
            </div>
        </div>
        <div class="row">
            <div class="grid_5"><img src="images/page-1_img01.jpg" alt=""></div>
            <div class="grid_7">
                <h3><?= $lectionOnMain->name ?></h3>
                <div class="short_description">
                    <?= $lectionOnMain->getCut('short_description', 300) ?>
                </div>
                <a href="<?= Url::toRoute(['article/view', 'url' => $lectionOnMain->url]) ?>" class="btn">Читать</a>
            </div>
        </div>
    </div>
</section>
<section class="well">
    <div class="container">
        <div class="row">
            <div class="grid_12">
                <h5 class="title_section_home">Лекции</h5>
            </div>
        </div>
        <div class="row">
            <div class="grid_6">
                <h3><?= $videoOnMain->name ?></h3>
                <div class="short_description">
                    <?= $videoOnMain->getCut('short_description', 300) ?>
                </div>
                <a href="<?= Url::toRoute(['lection/index']) ?>" class="btn btn-lg">Лекции</a>
            </div>
            <div class="grid_6">
                <a href="<?= Url::toRoute(['lection/view', 'url' => $videoOnMain->url]) ?>" class="thumb thumb_video mg-add fancybox.iframe">
                    <img src="<?= $videoOnMain->videoThumb ?>" alt="<?= $videoOnMain->name ?>">
                    <span class="thumb_overlay"></span>
                </a>
            </div>
        </div>
    </div>
</section>
<?/*
<section data-url="images/parallax1.jpg" data-mobile="true" class="well2 center parallax">
    <h2>Предложение<em>Высококачественное медицинское<br>обслуживание и лечение</em></h2>
</section>
*/?>
<section class="well ins1 bg-color">
    <div class="container">
        <div class="row">
            <div class="grid_12">
                <h5 class="title_section_home">Что дает хорошая медицинская информация:</h5>
            </div>
        </div>
        <ul class="product-list row">
            <li class="grid_6">
                <div class="box">
                    <div class="box_aside">
                        <div class="fa-medkit bg-secondary2"></div>
                    </div>
                    <div class="box_cnt__no-flow">
<!--                        <h4>Aculis lacinia</h4>-->
                        <p class="text-medium">
                            Позволяет эффективно бороться с болезнью: Вы знаете причины болезни и можете ее избежать; Вы знаете первые симптомы и обращаетесь за помощью вовремя; Вы знаете критерии качества лечения и способны его контролировать.
                        </p>
                    </div>
                </div>
                <div class="box">
                    <div class="box_aside">
                        <div class="fa-heart bg-primary"></div>
                    </div>
                    <div class="box_cnt__no-flow">
<!--                        <h4>Estproin dictum</h4>-->
                        <p class="text-medium">Убирает страх и тревогу: вместо страха неопределенности и неизвестности болезни Вы точно знаете угрозы, которые несет болезнь и как с ними можно бороться. Вы контролируете проблему и спокойны.</p>
                    </div>
                </div>
            </li>
            <li class="grid_6">
                <div class="box">
                    <div class="box_aside">
                        <div class="fa-group bg-secondary"></div>
                    </div>
                    <div class="box_cnt__no-flow">
<!--                        <h4>Proin dictum</h4>-->
                        <p class="text-medium">
                            Помогает оптимизировать расходы: Вы знаете, как избежать болезни; Вы обращаетесь за помощью при появлении первых симптомов; Вы знаете о возможностях лечения и его эффективности, поэтому Вам легче выбрать оптимальный вариант лечения.
                        </p>
                    </div>
                </div>
                <div class="box">
                    <div class="box_aside">
                        <div class="fa-ambulance bg-secondary3"></div>
                    </div>
                    <div class="box_cnt__no-flow">
<!--                        <h4>Lementum velit</h4>-->
                        <p class="text-medium">
                            Облегчает помощь близким в случае их болезни: все это относится не только к Вам, но и к тем, кто Вам дорог, кому Вы хотите помочь.
                        </p>
                    </div>
                </div>
            </li>
        </ul>
    </div>
</section>
<!--<section class="well bg-color">-->
<!--    <div class="container">-->
<!--        <h3>Gallery</h3>-->
<!--        <div class="row">-->
<!--            <div class="grid_4"><a href="images/page-1_img03_original.jpg" data-fancybox-group="1" class="thumb"><img src="images/page-1_img03.jpg" alt=""><span class="thumb_overlay"></span></a></div>-->
<!--            <div class="grid_4"><a href="images/page-1_img04_original.jpg" data-fancybox-group="1" class="thumb"><img src="images/page-1_img04.jpg" alt=""><span class="thumb_overlay"></span></a></div>-->
<!--            <div class="grid_4"><a href="images/page-1_img05_original.jpg" data-fancybox-group="1" class="thumb"><img src="images/page-1_img05.jpg" alt=""><span class="thumb_overlay"></span></a></div>-->
<!--        </div>-->
<!--        <div class="row">-->
<!--            <div class="grid_4"><a href="images/page-1_img06_original.jpg" data-fancybox-group="1" class="thumb"><img src="images/page-1_img06.jpg" alt=""><span class="thumb_overlay"></span></a></div>-->
<!--            <div class="grid_4"><a href="images/page-1_img07_original.jpg" data-fancybox-group="1" class="thumb"><img src="images/page-1_img07.jpg" alt=""><span class="thumb_overlay"></span></a></div>-->
<!--            <div class="grid_4"><a href="images/page-1_img08_original.jpg" data-fancybox-group="1" class="thumb"><img src="images/page-1_img08.jpg" alt=""><span class="thumb_overlay"></span></a></div>-->
<!--        </div>-->
<!--        <div class="btn_wr center"><a href="#" class="btn btn-lg">View all</a></div>-->
<!--    </div>-->
<!--</section>-->
<!--<section data-url="images/parallax2.jpg" data-mobile="true" class="well2 center parallax">-->
<!--    <h2>Current<em>diagnosis and treatment<br/>methods</em></h2>-->
<!--</section>-->
<?/*<section class="well3">
    <div class="container">
        <ul class="pricing-table row">
            <li class="grid_4">
                <h5>Plan 1</h5>
                <div class="price">45</div>
                <ul>
                    <li>Lorem ipsum dolor sit amet</li>
                    <li>consuer adipiscing elit. Praesent </li>
                    <li>vestibulum molestie lacus</li>
                    <li>Aenean nonummy hendrerit</li>
                </ul><a href="#" class="btn">Sign Up</a>
            </li>
            <li class="grid_4 active">
                <h5>Plan 2</h5>
                <div class="price">69</div>
                <ul>
                    <li>Lorem ipsum dolor sit amet</li>
                    <li>consuer adipiscing elit. Praesent </li>
                    <li>vestibulum molestie lacus</li>
                    <li>Aenean nonummy hendrerit</li>
                </ul><a href="#" class="btn">Sign Up</a>
            </li>
            <li class="grid_4">
                <h5>Plan 3</h5>
                <div class="price">55</div>
                <ul>
                    <li>Lorem ipsum dolor sit amet</li>
                    <li>consuer adipiscing elit. Praesent </li>
                    <li>vestibulum molestie lacus</li>
                    <li>Aenean nonummy hendrerit</li>
                </ul><a href="#" class="btn">Sign Up</a>
            </li>
        </ul>
    </div>
</section>*/?>
