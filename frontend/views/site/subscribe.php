<?
?>

<section>
    <div class="container custom_site_subscribe">
        <div class="row">
            <div class="grid_7">
                <iframe width="100%" height="315"
                        src="https://www.youtube.com/embed/efmSQrnx7WM">
                </iframe>
            </div>

            <div class="grid_5">
                <?= $this->render('subscribe_form') ?>
            </div>
        </div>
    </div>
</section>
