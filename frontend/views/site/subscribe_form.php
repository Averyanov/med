<?

use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;

?>

<!--<h4>Подпишись на рассылку</h4>-->
<? ActiveForm::begin([
    'action'  => Url::toRoute(['site/subscribe']),
    'id'      => 'post_card',
    'options' => [
        'class' => 'mailform rd-mailform',
    ]
]); ?>

<label for="username">
    <?=
    Html::textInput(
        'username',
        false,
        ['placeholder' => 'Имя',
         'autocomplete' => 'off']
    )
    ?>
</label>

<label for="userphone">
    <?=
    Html::textInput(
        'username',
        false,
        ['placeholder' => 'Телефон',
            'autocomplete' => 'off']
    )
    ?>
</label>

<label for="useremail">
    <?=
    Html::textInput(
        'useremail',
        false,
        ['placeholder' => 'Email']
    )
    ?>
</label>

<div style="text-align: center; margin-top: 20px;">
    <?=
    Html::textInput(null, 'Оформить', ['type' => 'submit', 'class' => 'btn mfProgress'])
    ?>
</div>

<? ActiveForm::end(); ?>

