<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<section>
    <div class="container">
        <div class="row">
            <div class="site-login">
                <h4 class="tittle"><?= Html::encode($this->title) ?></h4>
                <p style="margin-bottom: 10px">Пожалуйста заполните поля ниже:</p>

                <?php
                $form = ActiveForm::begin([
                    'id' => 'login-form',
                ]);
                ?>

                <label for="username">
                    <?= $form->field($model, 'username')->textInput([
                        'autofocus'   => true,
                        'placeholder' => 'Логин',
                        'class'       => 'input-text'
                    ])->label(false)
                    ?>
                </label>

                <label for="password">
                    <?= $form->field($model, 'password')->passwordInput([
                        'placeholder' => 'Пароль',
                        'class'       => 'input-text'
                    ])->label(false) ?>
                </label>

                <div style="position: relative">
                <div style="font-size: 90%; line-height: 17px; display: inline-block;">
                    <?= $form->field($model, 'rememberMe')->checkbox()->label('запомнить меня') ?>
                </div>
                <div class="forget_pass" style="">
                    <?= Html::a('Забыли пароль?', ['site/request-password-reset']) ?>
                </div>
                </div>

                <div class="btn-wrapper">
                    <?= Html::submitButton('Войти', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                </div>

                <?php ActiveForm::end(); ?>

                <div style="text-align: center; font-style: italic; font-size: 80%;">
                    Еще не зарегистрированы? <a href="/site/signup" style="border-bottom: 1px dotted;">Перейти к регистрации</a>
                </div>
            </div>
        </div>
    </div>
</section>
