<?
use pistol88\liqpay\widgets\PaymentForm;
use yii\widgets\ListView;
?>
<?
$this->title = 'Корзина';
$this->params['breadcrumbs'][] = $this->title;
?>
<section>
    <div class="container">
        <div class="row">
            <div class="grid_9">
                <?=
                ListView::widget([
                    'dataProvider' => $dataProvider,
                    'layout' => '{items}',
                    'options' => ['class' => 'row'],
                    'itemOptions' => ['class' => 'grid_9'],
                    'itemView' => '/lection/cart_item',
                    'emptyText' => 'Корзина пуста',
                    'emptyTextOptions' => ['tag' => 'h2']
                ])
                ?>
            </div>
            <div class="grid_3">
                <?

                if($payment)
                    echo 'Платежные системы: '
                        .$this->render('/payment/pay_pal', ['payment' => $payment])
                ?>
            </div>
            <?=
            PaymentForm::widget([
                'autoSend' => false,
                'orderModel' => $cart,
                'description' => "Оплата заказа #$cart->id."
            ]);?>
        </div>
    </div>
</section>
