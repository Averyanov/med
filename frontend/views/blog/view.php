<?
use yii\widgets\ListView;
//use frontend\assets\ShadowAsset;
?>
<?
$this->title = $model->meta->title;
$this->params['breadcrumbs'][] = ['label' => 'Блоги', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

//ShadowAsset::register($this);
?>
<div class="row">
    <div class="col-md-12">
        <h1><?= $model->name ?></h1>
        <p class="h4 text-info"><?= $model->slogan ?></p>
    </div>
</div>
<div class="row">
    <div class="col-md-9">
        <?=
        ListView::widget([
            'dataProvider' => $dataProvider,
            'layout' => '{items}',
            'options' => ['class' => 'row'],
            'itemOptions' => ['class' => 'col-md-12 blog-item-full'],
            'itemView' => '/article/blog_item_full'
        ])
        ?>
    </div>
    <div class="col-md-3">
        <?=
        ListView::widget([
            'dataProvider' => $lectionDataProvider,
            'layout' => '{items}',
            'options' => ['class' => 'row'],
            'itemOptions' => ['class' => 'col-md-10 col-md-offset-1 lection-side'],
            'itemView' => '/lection/side'
        ])
        ?>
    </div>
</div>
