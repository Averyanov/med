<?
use yii\widgets\ListView;
?>
<?
$this->title = 'Блоги';
$this->params['breadcrumbs'][] = $this->title;
?>
<?=
ListView::widget([
    'dataProvider' => $dataProvider,
    'layout' => '{items}',
    'options' => [
        'class' => 'row'
    ],
    'itemOptions' => [
        'class' => 'col-md-12'
    ],
    'itemView' => function($model, $index){
        return $this->render('item', [
                'model' => $model,
                'index' => $index
            ]
        );
    }
])
?>


