<section class="well <?= $index % 2 ? '' : 'ins2 bg-color' ?>">
    <div class="container">
        <div class="row">
            <div class="grid_6">
                <?= $this->render('/article/components/'.($index % 2 ? 'image' : 'description'), ['model' => $model]) ?>
            </div>
            <div class="grid_6">
                <?= $this->render('/article/components/'.($index % 2 ? 'description' : 'image'), ['model' => $model]) ?>
            </div>
        </div>
    </div>
</section>
