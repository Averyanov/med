<?
use yii\helpers\Html;
?>

<?=
Html::beginForm([''], 'post')
. Html::submitButton(
    Html::tag('span', 'LiqPay (не работает)', ['class' => 'text-capitalize text-muted']),
    ['class' => 'btn btn-link']
)
. Html::endForm()
?>
