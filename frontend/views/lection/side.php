<?
use yii\helpers\Url;
use yii\helpers\Html;
?>
<br>
<a href='<?= Url::toRoute(['lection/view', 'url' => $model->url]) ?>'>
    <strong><?= $model->name ?></strong>
    <?= Html::img($model->videoThumb, ['class' => 'side-lection-thumb']) ?>
</a>
