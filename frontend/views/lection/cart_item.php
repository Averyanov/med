<?
use yii\widgets\ActiveForm;
use yii\helpers\Html;
?>
<br>
<section>
    <div class="container">
        <div class="row">
            <div class="grid_3">
                <img src='<?= $model->videoThumb ?>' alt='<?= $model->name ?>' class="lection-cart">
            </div>
            <div class="grid_9">
                <p class="h4"><?= $model->name ?></p>
                <? ActiveForm::begin(['action' => ['cart/remove']])?>
                <?= Html::hiddenInput('id', $model->id) ?>
                <?= Html::submitButton('Удалить', ['class' => 'btn btn-danger']) ?>
                <? ActiveForm::end()?>
            </div>
        </div>
        <br>
    </div>
</section>
