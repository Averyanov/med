<?
use yii\widgets\ListView;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
?>
<?
$this->title = $model->meta->title;
$this->params['breadcrumbs'][] = ['label' => 'Лекции', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<section>
    <div class="container">
        <div class="row">
            <div class="grid_8">
                <div class="row">
                    <div class="col-md-12">
                        <h1><?= $model->name ?></h1>
                        <p class="text-muted"><?= date('Y-m-d h:m:i') ?></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <img src='<?= $model->videoThumb ?>' alt='<?= $model->name ?>' class='lection-preveiw'>
                        <div><?= $model->full_description ?></div>
                    </div>
                </div>
            </div>
            <div class="grid_1"></div>
            <div class="grid_3">
                <?=
                ListView::widget([
                    'dataProvider' => $dataProvider,
                    'layout' => '{items}',
                    'options' => ['class' => 'row'],
                    'itemOptions' => ['class' => 'col-md-10 col-md-offset-1 lection-side'],
                    'itemView' => '/lection/side'
                ])
                ?>
            </div>
            <div class="grid_8">
                <p class="h4">Получить доступ к видео можно как угодно</p>
                <p class="h5">К примеру, внести оплату на одну из многочисленных платежных систем <span class="text-muted">(кроме заблокированных)</span></p>
                <img src="https://i.io.ua/img_su/large/0233/37/02333770_n1.jpg" alt="" width="100%">
            </div>
            <div class="grid_3">
                <? $form = ActiveForm::begin(['action' => ['cart/add']]) ?>
                <?= Html::hiddenInput('id', $model->id) ?>
                <?= Html::submitButton('Получить доступ', ['class' => 'btn btn-primary']) ?>
                <? ActiveForm::end() ?>
            </div>
        </div>
    </div>
</section>