<?
use yii\helpers\Url;
?>

<img src='<?= $model->videoThumb ?>' alt='<?= $model->name ?>'>
<h5 class="fixed-height-90">
    <a href="<?= Url::toRoute(['lection/view', 'url' => $model->url]) ?>"><?= $model->name ?>.</a>
</h5>
<p class="short_description"><?= $model->getCut('short_description', 200) ?></p>
<a href="<?= Url::toRoute(['lection/view', 'url' => $model->url]) ?>" class="btn2">Читать</a>