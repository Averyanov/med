<?
use yii\widgets\ListView;
?>
<?
$this->title = 'Лекции';
//$this->params['breadcrumbs'][] = $this->title;
?>

<section class="well ins3 mobile-center">
    <div class="container">
        <h3>Лекции:</h3>

        <?=
        ListView::widget([
            'dataProvider' => $dataProvider,
            'layout' => '{items}',
            'options' => ['class' => 'row'],
            'itemOptions' => ['class' => 'grid_4'],
            'itemView' => 'index_item'
        ])
        ?>

    </div>
</section>
<section data-url="images/parallax7.jpg" data-mobile="true" class="well2 center parallax">
    <h2>Некий<em>банер<br/>о чем-то!</em></h2>
</section>