<?
use yii\helpers\Url;
use yii\helpers\Html;
?>
<p class='h2'><?= $model->name ?></p>

<p class='text-info'><?= $model->getCut('short_description', 100) ?></p>
<p class='lead'><?= $model->getCut('full_description', 300) ?></p>
<?= Html::img($model->videoThumb, ['class' => 'main-lection-thumb']) ?>
<p>
    <a class="btn btn-default" href='<?= Url::toRoute(['lection/view', 'url' => $model->url]) ?>'>
        Далее &raquo;
    </a>
</p>