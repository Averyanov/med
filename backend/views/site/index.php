<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';

use yii\helpers\Url;
?>
<div class="site-index">

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <h2>Статические страницы</h2>

                <p><a class="btn btn-default" href='<?= Url::toRoute(['page/index']) ?>'>&raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Авторы</h2>

                <p><a class="btn btn-default" href='<?= Url::toRoute(['author/index']) ?>'>&raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Блоги</h2>

                <p><a class="btn btn-default" href='<?= Url::toRoute(['blog/index']) ?>'>&raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Лекции</h2>

                <p><a class="btn btn-default" href='<?= Url::toRoute(['lection/index']) ?>'>&raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Настройки</h2>

                <p><a class="btn btn-default" href='<?= Url::toRoute(['settings/index']) ?>'>&raquo;</a></p>
            </div>
        </div>

    </div>
</div>
