<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Блоги';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="blog-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Создать Блог', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'author_id',
                'label' => 'Автор',
                'value' => function($model){ return $model->author->name; }
            ],
            'name',
            [
                'label' => 'Статьи',
                'format' => 'html',
                'value' => function($model){ return Html::ul($model->articleList, ['encode' => false]); }
            ],
            'url:url',
            // 'slogan',

            ['class' => 'yii\grid\ActionColumn'],
            [
                'label' => 'Добавить Статью',
                'format' => 'html',
                'value' => function($model){ return Html::a('+', ['article/create', 'blog' => $model->id], ['class' => 'btn btn-success btn-xs']); }

            ]
        ],
    ]); ?>
</div>
