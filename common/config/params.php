<?php
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'supportName' => 'Support Name',
    'user.passwordResetTokenExpire' => 3600,
];
