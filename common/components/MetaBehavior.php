<?

namespace common\components;

use Yii;
use yii\base\Behavior;
use yii\db\ActiveRecord;
use common\models\Meta;
use yii\helpers\VarDumper;
use common\models\Page;
use yii\validators\Validator;

class MetaBehavior extends Behavior
{

    public
        $title,
        $keywords,
        $description;

    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_VALIDATE => 'beforeValidate',
            ActiveRecord::EVENT_AFTER_DELETE    => 'afterDelete'
        ];
    }

    public function beforeValidate()
    {
        return $this->bind();
    }

    public function afterDelete()
    {
        return $this->owner->meta->delete();
    }

    // TODO add desc after end
    public function bind()
    {
        $model = $this->owner->isNewRecord ? new Meta() : $this->owner->meta;

        $model->load(Yii::$app->request->post(), $this->owner->formName());

        $model->save();

        $this->owner->meta_id = $model->id;

        return $model->validate();
    }

    // init object of Behavior
    // from Meta, wich linked to object->owner
    // return this->owner;
    public function meta()
    {
        foreach ($this->getMeta() as $attribute => $value)
        {
            $this->{$attribute} = $this->hasProperty($attribute) ? $value : false;
        }

        return $this->owner;
    }

    // Return Meta attributes of Behavior object->owner
    // as array except some attributes
    public function getMeta(array $except = [])
    {
        $except[] = 'id';
        $except = array_unique($except);

        return array_filter(
            $this->owner->getMeta()->asArray()->one(),
            function($key) use ($except) {return !in_array($key, $except);},
            ARRAY_FILTER_USE_KEY
        );
    }
}