<?

namespace common\components;

use Yii;
use yii\base\Behavior;
use yii\db\ActiveRecord;
use yii\web\UploadedFile;
use yii\helpers\Url;
use yii\imagine\Image;
use yii\helpers\StringHelper;

class ImageBehavior extends Behavior
{
    public
        $imageInstance,
        $oldFile = null;

    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_VALIDATE => 'beforeValidate',
            ActiveRecord::EVENT_AFTER_INSERT => 'afterInsert',
            ActiveRecord::EVENT_AFTER_UPDATE => 'afterUpdate',
            ActiveRecord::EVENT_BEFORE_DELETE => 'beforeDelete'
        ];
    }

    public function uploadImage()
    {
        $image = UploadedFile::getInstance($this->owner, 'image');

        if($image)
        {
            $this->oldFile = $this->owner->isNewRecord
                ? null
                : $this->owner->getOldAttribute('image');

            $this->owner->_image = $image->name;
            $this->imageInstance = $image;
        }
        else
            $this->owner->image = $this->owner->getOldAttribute('image') . "";

    }

    public function deleteImage()
    {
        return unlink(ltrim($this->owner->image, '/'));
    }

    public function preparePath($extension)
    {
        return Yii::$app->params['uploadPath' . StringHelper::basename($this->owner->className())] . Yii::$app->getSecurity()->generateRandomString(6) . '.' .$extension;
    }

    public function getOptions()
    {
        return [
            'options'=>['accept'=>'image/*'],
            'pluginOptions'=>[
                'allowedFileExtensions'=>['jpg','gif','png', 'jpeg'],
                'previewFileType' => 'any',
                'initialPreview' => ($this->owner->isNewRecord || empty($this->owner->image)) ? false : Url::to($this->owner->image, true),
                'initialPreviewAsData'=>true,
                'showRemove' => false,
                'showUpload' => false
            ]
        ];
    }

    // events

    public function beforeValidate()
    {
        return $this->uploadImage();
    }

    public function afterInsert()
    {
        Image::thumbnail(
            $this->imageInstance->tempName,
            $this->owner->img_size['width'],
            $this->owner->img_size['height']
        )
            ->save(ltrim($this->owner->image, '/'), ['quality' => 80]);
    }

    public function afterUpdate()
    {
        if($this->imageInstance)
        {
            $this->afterInsert();

            if($this->oldFile)
                unlink(ltrim($this->oldFile, '/'));
        }
    }

    public function beforeDelete()
    {
        return $this->deleteImage();
    }

    // properties

    public function set_Image($imageName)
    {
        $file_name = explode(".", $imageName);
        $ext = end($file_name);

        $path = $this->preparePath($ext);

        $this->owner->image = $path;

        return $path;
    }

}
