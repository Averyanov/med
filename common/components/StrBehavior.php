<?

namespace common\components;

use yii\base\Behavior;

class StrBehavior extends Behavior
{
    public function getCut($attribute, $length)
    {
        return mb_substr($this->owner->{$attribute}, 0, $length, 'utf-8').'...';
    }
}