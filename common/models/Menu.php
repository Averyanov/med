<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "menu".
 *
 * @property integer $id
 * @property integer $owner_id
 * @property string $label
 * @property string $route
 * @property integer $status
 * @property integer $is_html
 * @property integer $sort_order
 * @property integer $a4g           => avaliable for guest
 */
class Menu extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'menu';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['label', 'route', 'status', 'sort_order'], 'required'],
            [['owner_id', 'status', 'sort_order', 'a4g', 'is_html'], 'integer'],
            [['label', 'route'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'            => 'ID',
            'owner_id'      => 'Owner ID',
            'label'         => 'Label',
            'route'         => 'Route',
            'status'        => 'Status',
            'sort_order'    => 'Sort Order',
            'a4g'           => 'Виден ли простым смертным?'
        ];
    }

    public function getDataProvider($owner_id = null)
    {
        $query = $this::find()->where(['owner_id' => $owner_id])->orderBy('sort_order');
        
        if (Yii::$app->user->isGuest)
        {
            $query->andWhere(['a4g' => true]);
        }

        return new ActiveDataProvider([ 'query' => $query ]);
    }
}
