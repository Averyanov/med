<?php

namespace common\models;

use Yii;
use common\components\MetaBehavior;
use common\components\ImageBehavior;
use common\components\StrBehavior;

/**
 * This is the model class for table "author".
 *
 * @property integer $id
 * @property integer $meta_id
 * @property string $name
 * @property string $surname
 * @property string $url
 * @property string $about
 *
 * @property Meta $meta
 * @property Blog[] $blogs
 */
class Author extends \yii\db\ActiveRecord
{
    public $img_size = [
        'width' => 165,
        'height' => 165
    ];
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'author';
    }

    public function behaviors()
    {
        return [
            ImageBehavior::className(),
            MetaBehavior::className(),
            StrBehavior::className()
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                'image',
                'required',
                'when' => function($model){return $model->isNewRecord; },
                'whenClient' => '
                    function(){
                        return document.
                            getElementsByClassName("file-input")[0]
                            .classList
                            .contains("file-input-new");
                    }'
            ],
            [['image'], 'file', 'extensions'=>'jpg, gif, png', 'maxSize' => 1024 * 1024],
            [['meta_id', 'name', 'url', 'about'], 'required'],
            [['meta_id'], 'integer'],
            [['about'], 'string'],
            [['name', 'surname', 'url'], 'string', 'max' => 255],
            [['meta_id'], 'exist', 'skipOnError' => true, 'targetClass' => Meta::className(), 'targetAttribute' => ['meta_id' => 'id']],
            ['url', 'unique', 'targetAttribute' => ['url'], 'message' => 'Url должна быть уникальна.'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'meta_id' => 'Meta ID',
            'name' => 'Name',
            'surname' => 'Surname',
            'url' => 'Url',
            'about' => 'About',
            'image' => 'Изображение'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMeta()
    {
        return $this->hasOne(Meta::className(), ['id' => 'meta_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBlogs()
    {
        return $this->hasMany(Blog::className(), ['author_id' => 'id']);
    }
}
