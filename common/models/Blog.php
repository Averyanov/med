<?php

namespace common\models;

use Yii;
use common\components\MetaBehavior;
use common\models\Article;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * This is the model class for table "blog".
 *
 * @property integer $id
 * @property integer $meta_id
 * @property integer $author_id
 * @property string $name
 * @property string $url
 * @property string $slogan
 *
 * @property Meta $meta
 * @property Author $author
 */
class Blog extends \yii\db\ActiveRecord
{
    private static $owner_id = 1;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'blog';
    }

    public function behaviors()
    {
        return [MetaBehavior::className()];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['meta_id', 'author_id', 'name', 'url', 'slogan'], 'required'],
            [['meta_id', 'author_id'], 'integer'],
            [['name', 'url', 'slogan'], 'string', 'max' => 255],
            [['meta_id'], 'exist', 'skipOnError' => true, 'targetClass' => Meta::className(), 'targetAttribute' => ['meta_id' => 'id']],
            [['author_id'], 'exist', 'skipOnError' => true, 'targetClass' => Author::className(), 'targetAttribute' => ['author_id' => 'id']],
            ['url', 'unique', 'targetAttribute' => ['url'], 'message' => 'Url должна быть уникальна.'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'meta_id' => 'Meta ID',
            'author_id' => 'Author ID',
            'name' => 'Название',
            'url' => 'Адрес',
            'slogan' => 'Слоган',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMeta()
    {
        return $this->hasOne(Meta::className(), ['id' => 'meta_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(Author::className(), ['id' => 'author_id']);
    }

    public function getArticles()
    {
        return $this->hasMany(Article::className(), ['blog_id' => 'id']);
    }

    public static function byOwner()
    {
        return Blog::findOne(['id' => static::$owner_id]);
    }

    public function getArticleList()
    {
        return ArrayHelper::getColumn($this->getArticles()->all(), function($item){ return Html::a($item->name, ['article/view', 'id' => $item->id]); });
    }
}
