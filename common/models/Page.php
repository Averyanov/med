<?php

namespace common\models;

use Yii;
use common\components\MetaBehavior;

/**
 * This is the model class for table "page".
 *
 * @property integer $id
 * @property integer $meta_id
 * @property string $url
 * @property string $content
 *
 * @property Meta $meta
 */
class Page extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'page';
    }

    public function behaviors()
    {
        return [MetaBehavior::className()];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['meta_id', 'url', 'content'], 'required'],
            [['meta_id'], 'integer'],
            [['content'], 'string'],
            [['url'], 'string', 'max' => 255],
            ['url', 'unique', 'targetAttribute' => ['url'], 'message' => 'Url должна быть уникальна.'],
            [['meta_id'], 'exist', 'skipOnError' => true, 'targetClass' => Meta::className(), 'targetAttribute' => ['meta_id' => 'id']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'url' => 'Адрес',
            'content' => 'Контент',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMeta()
    {
        return $this->hasOne(Meta::className(), ['id' => 'meta_id']);
    }
}
