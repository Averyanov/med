<?php

namespace common\models;

use Yii;
use common\components\MetaBehavior;
use common\components\StrBehavior;

/**
 * This is the model class for table "lection".
 *
 * @property integer $id
 * @property integer $meta_id
 * @property string $name
 * @property string $url
 * @property string $video_id
 * @property string $short_description
 * @property string $full_description
 *
 * @property Meta $meta
 */
class Lection extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lection';
    }

    public function behaviors()
    {
        return [
            MetaBehavior::className(),
            StrBehavior::className()
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['meta_id', 'name', 'url', 'video_id', 'short_description', 'full_description'], 'required'],
            [['meta_id'], 'integer'],
            [['short_description', 'full_description'], 'string'],
            [['name', 'url', 'video_id'], 'string', 'max' => 255],
            [['meta_id'], 'exist', 'skipOnError' => true, 'targetClass' => Meta::className(), 'targetAttribute' => ['meta_id' => 'id']],
            ['url', 'unique', 'targetAttribute' => ['url'], 'message' => 'Url должна быть уникальна.'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'meta_id' => 'Meta ID',
            'name' => 'Name',
            'url' => 'Url',
            'video_id' => 'Video ID',
            'short_description' => 'Short Description',
            'full_description' => 'Full Description',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMeta()
    {
        return $this->hasOne(Meta::className(), ['id' => 'meta_id']);
    }

    public function getVideoThumb()
    {
//        return null;
        return "https://img.youtube.com/vi/$this->video_id/0.jpg";
    }
}
