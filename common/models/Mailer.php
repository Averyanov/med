<?
namespace common\models;
?>

<?
use Yii;

use SendGrid;
use SendGrid\Email;
use SendGrid\Mail;
use SendGrid\Content;
use yii\helpers\VarDumper;

?>

<?
class Mailer
{
    private
        $ownername,
        $owneremail,
        $username,
        $useremail,
        $from,
        $to,
        $subject,
        $content,
        $message,
        $mail,
        $sg,
        $response;

    public function compose($subject ,$options)
    {
        $this->ownername = Yii::$app->params['supportName'];
        $this->owneremail = Yii::$app->params['supportEmail'];

        $template = $options['template'];

        $this->message = Yii::$app->controller->renderPartial("/mail/$template", [
            'name' => $this->username
        ]);
        $this->subject = $subject;

        $this->username = $this->username;
        $this->useremail = $this->useremail;

        $this->from = new Email($this->ownername, $this->owneremail);
        $this->to = new Email($this->username, $this->useremail);

        $this->content = new Content("text/html", $this->message);
        $this->sg = new SendGrid($this->auth_key);

        $this->mail = new Mail(
            $this->from,
            $this->subject,
            $this->to,
            $this->content
        );

        return $this;
    }

    public function send()
    {
        $this->response = $this
            ->sg
            ->client
            ->mail()
            ->send()
            ->post($this->mail);

        return $this->response;

        // return obj with methods => [statusCode(), headers(), body()]
    }

    public function load($args)
    {
        $this->username = isset($args['username']) ? $args['username'] : '';
        $this->useremail = isset($args['useremail']) ? $args['useremail'] : '';

        return $this;
    }

    public function getUsername(){
        return $this->username;
    }

    public function getUseremail(){
        return $this->useremail;
    }
}
?>