<?php

namespace common\models;

use Yii;
use common\components\MetaBehavior;
use common\components\StrBehavior;
use common\components\ImageBehavior;

/**
 * This is the model class for table "article".
 *
 * @property integer $id
 * @property integer $meta_id
 * @property integer $blog_id
 * @property string $name
 * @property string $h1
 * @property string $short_description
 * @property string $full_description
 * @property string $url
 * @property string $content
 *
 * @property Meta $meta
 */
class Article extends \yii\db\ActiveRecord
{
    public $img_size = [
        'width' => 515,
        'height' => 293
    ];
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'article';
    }

    public function behaviors()
    {
        return [
            ImageBehavior::className(),
            MetaBehavior::className(),
            StrBehavior::className()
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                'image',
                'required',
                'when' => function($model){return $model->isNewRecord; },
                'whenClient' => '
                    function(){
                        return document.
                            getElementsByClassName("file-input")[0]
                            .classList
                            .contains("file-input-new");
                    }'
            ],
            [['image'], 'file', 'extensions'=>'jpg, jpeg, gif, png', 'maxSize' => 1024 * 1024 * 5],
            [['meta_id', 'blog_id', 'name', 'h1', 'short_description', 'full_description', 'url', 'content'], 'required'],
            [['meta_id', 'blog_id'], 'integer'],
            [['content'], 'string'],
            [['name', 'h1', 'url'], 'string', 'max' => 255],
            [['meta_id'], 'exist', 'skipOnError' => true, 'targetClass' => Meta::className(), 'targetAttribute' => ['meta_id' => 'id']],
            ['url', 'unique', 'targetAttribute' => ['url'], 'message' => 'Url должна быть уникальна.'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'meta_id' => 'Meta ID',
            'blog_id' => 'Blog ID',
            'name' => 'Название',
            'h1' => 'Заголовок',
            'short_description' => 'Краткое описание',
            'full_description' => 'Полное описание',
            'url' => 'Адрес',
            'content' => 'Контент',
            'image' => 'Изображение'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMeta()
    {
        return $this->hasOne(Meta::className(), ['id' => 'meta_id']);
    }

    public function getBlog()
    {
        return $this->hasOne(Blog::className(), ['id' => 'blog_id']);
    }
}
