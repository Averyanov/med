<?php

namespace common\models;

use Yii;
use pistol88\liqpay\interfaces\Order;
use yii\helpers\VarDumper;

/**
 * This is the model class for table "cart".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $status
 *
 * @property User $user
 * @property CartItem[] $cartItems
 */
class Cart extends \yii\db\ActiveRecord implements Order
{
    private $codes = ['no'=>0,'yes'=>1];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cart';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'status'], 'required'],
            [['user_id', 'status'], 'integer'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCartItems()
    {
        return $this->hasMany(CartItem::className(), ['cart_id' => 'id']);
    }

    public function getLections()
    {
        return $this
            ->hasMany(Lection::className(), ['id' => 'lection_id'])
            ->viaTable('cart_item',['cart_id' => 'id']);
    }

    public function getId()
    {
        return $this->id;
    }

    public function getCost()
    {
        return 44;
    }

    public function setPaymentStatus($status)
    {
        return $this->status = key_exists($status,$this->codes)
            ? $this->codes[$status]
            : null;
    }
}
